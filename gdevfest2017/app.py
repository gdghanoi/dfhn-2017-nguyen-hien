from flask import Flask
import uuid

app = Flask(__name__)
app.config.from_object('config')

import pyrebase

config = {
    "apiKey": "AIzaSyDvQ4Bi-HhWEikZB3pPGj3pdFiNdWjSxjk",
    "authDomain": "gdevfest2017.firebaseapp.com",
    "databaseURL": "https://gdevfest2017.firebaseio.com",
    "projectId": "gdevfest2017",
    "storageBucket": "gdevfest2017.appspot.com",
    "messagingSenderId": "252271790527"
 }

firebase = pyrebase.initialize_app(config)
db = firebase.database()


@app.route("/")
def index():
	data = {"uuid":str(uuid.uuid1())}
	db.child("users").push(data)
	return "Ok"

