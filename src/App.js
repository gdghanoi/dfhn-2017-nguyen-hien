import React from 'react'
import './App.css'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from './reducers/rootReducers'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import Main from './components/Main'
import FormLogin from './components/FormLogin'
import Logout from './components/LogOut'
import History from './components/History'
import Blog from './components/Blog'
import init from './firebase'
init()

const store = createStore(rootReducer)

class App extends React.Component {
	render() {
		return <Provider store={store}>
			<Router history={browserHistory}>
				<Route path="/" component={Main}>
					<IndexRoute component={Blog}/>
					<Route path="/login" component={FormLogin} />
					<Route path="/logout" component={Logout} />
					<Route path="/history" component={History} />
				</Route>
			</Router>
		</Provider>
	}
}

export default App
