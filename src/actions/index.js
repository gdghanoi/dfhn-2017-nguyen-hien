import { ActionTypes } from '../constants'

export function setUser(user) {
	return {
		type: ActionTypes.SET_USER,
		user
	}
}
export function showFormLogin(show) {
	return {
		type: ActionTypes.SHOW_FORM_LOGIN,
		show
	}
}