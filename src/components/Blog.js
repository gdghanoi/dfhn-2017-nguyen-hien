import React from 'react'
import { connect } from 'react-redux'
import Order from './Order'
import Table from './Table'
import Food from './Food'
import Confirm from './Confirm'
import { Link, browserHistory } from 'react-router'


class Blog extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			step: 1
		}
		this.order = {}
	}
	componentDidMount() {
		if(localStorage.getItem('isLogin')==='false') {
			window.location = '/login'
		}
	}
	render() {
		let content = null
		switch (this.state.step) {
		case 1:
			content = <Order setOrder={order => {
				this.order = order
				this.setState({ step: 2 })
			}} />
			break
		case 2:
			content = <Table order={this.order} setTable={tableId => {
				this.table = tableId
				this.setState({ step: 3 })
			}} />
			break
		case 3:
			content = <Food setFood={menu => {
				this.food = menu
				this.setState({ step: 4 })
			}} />
			break
		case 4:
			content = <Confirm order={this.order} table={this.table} food={this.food} />
			break
		default: content = <Order setOrder={order => {
			this.order = order
			this.setState({ step: 2 })
		}} />
		}
		return <div className="animsition">

			<div className="mdl-layout mdl-js-layout mdl-layout--overlay-drawer-button">
				<header className="mdl-layout__header mdl-layout__header--waterfall">
					<div className="mdl-layout__header-row">

						<div className="mdl-layout-spacer"></div>

						<button id="top-header-menu" className="mdl-button mdl-js-button mdl-button--icon">
							<i className="material-icons">Restaurant</i>
						</button>
					</div>
				</header>

				<ul className="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="top-header-menu">
					<a href="login.html" className="animsition-link"><li className="mdl-menu__item">Login</li></a>
					<a href="messages.html" className="animsition-link"><li className="mdl-menu__item mdl-badge" data-badge="4">Messages</li></a>
					<a href="player.html" className="animsition-link"><li className="mdl-menu__item">Player</li></a>
					<a href="calendar.html" className="animsition-link"><li className="mdl-menu__item">Calendar</li></a>
					<a href="settings.html" className="animsition-link"><li className="mdl-menu__item">Settings</li></a>
				</ul>

				<div className="mdl-layout__drawer">

					<div className="mdl-card mdl-shadow--2dp mdl-color--primary mdl-color-text--blue-grey-50 drawer-profile">
						<div className="mdl-card__title user">
							<img src="img/user.jpg" alt="" />
							<span className="user-mail">{this.props.email}</span>
						</div>
					</div>

					<nav className="mdl-navigation">
						<a className="mdl-navigation__link animsition-link" href="/"><i className="material-icons">home</i><span>Order Now</span></a>
						<a className="mdl-navigation__link animsition-link" href="profile.html"><i className="material-icons">account_circle</i><span>Profile</span></a>
						<a className="mdl-navigation__link animsition-link" href="/history"><i className="material-icons">library_books</i><span>History</span></a>
						<div className="drawer-separator"></div>
						<a className="mdl-navigation__link mdl-collapse__button" href='/logout' ><i className="material-icons">apps</i>
							<span>Logout</span>
						</a>
					</nav>
				</div>


				<main className="mdl-layout__content">
					{content}
				</main>
			</div>
		</div >
	}
}
function mapStateToProps(state) {
	return { email: state.user.email }
}

export default connect(mapStateToProps)(Blog)