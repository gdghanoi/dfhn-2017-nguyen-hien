import React from 'react'
import { connect } from 'react-redux'
import firebase from 'firebase'

class Confirm extends React.Component {

	onClickConfirm() {
		const foods = this.props.food[1] || []
		const drinks = this.props.food[2] || []
		const listFood = []
		let total = 0
		
		for (const food of foods) {
			if (!food) continue
			if (!food.quantity || food.quantity === 0) continue
			listFood.push(food)
			total += food.price * food.quantity
			
		}
		for (const food of drinks) {
			if (!food) continue
			if (!food.quantity || food.quantity === 0) continue
			listFood.push(food)
			total += food.price * food.quantity
			
		}
		firebase.database().ref(`/tables/${this.props.table}/orders`).once('value')
			.then(snap => snap.val())
			.then(async data => {
				if (!data || data.length == 0) {
					data = []
					data[0] = {
						date: this.props.order.date,
						time_id: this.props.order.time,
						number: this.props.order.number,
						foods: listFood
					}
				} else {
					data[data.length] = {
						date: this.props.order.date,
						time_id: this.props.order.time,
						number: this.props.order.number,
						foods: listFood
					}
				}
				await firebase.database().ref(`/tables/${this.props.table}/orders`).set(data)
				await firebase.database().ref(`/user/${localStorage.getItem('uid')}/orders`).push({
					date: this.props.order.date,
					time_id: this.props.order.time,
					number: this.props.order.number,
					foods: listFood,
					total
				})
				window.location = '/history'
			})
	}
	render() {
		const listItem = []
		const foods = this.props.food[1] || []
		const drinks = this.props.food[2] || []
		let total = 0
		for (const food of foods) {
			if (!food) continue
			if (!food.quantity || food.quantity === 0) continue
			total += food.price * food.quantity
			listItem.push(<tr>
				<td className="mdl-data-table__cell--non-numeric" style={{ paddingLeft: '16px' }}>
					<div className="mdl-color-text--grey-600">
						<div className="minilogo user2 available" style={{ marginRight: '16px' }}></div>
						<div className="card-author">
							<p><strong>{food.food_name}</strong></p>
							<p>{`x${food.quantity}`}</p>
						</div>
					</div>
				</td>
				<td>{`${food.price * food.quantity}$`}</td>
			</tr>)
		}
		for (const drink of drinks) {
			if (!drink) continue
			if (!drink.quantity || drink.quantity === 0) continue
			total += drink.price * drink.quantity

			listItem.push(<tr>
				<td className="mdl-data-table__cell--non-numeric" style={{ paddingLeft: '16px' }}>
					<div className="mdl-color-text--grey-600">
						<div className="minilogo user2 available" style={{ marginRight: '16px' }}></div>
						<div className="card-author">
							<p><strong>{drink.food_name}</strong></p>
							<p>{`x${drink.quantity}`}</p>
						</div>
					</div>
				</td>
				<td>{`${drink.price * drink.quantity}$`}</td>
			</tr>)
		}

		return <div className="setting">
			<p className="title">Your order</p>

			<div className="contact-about" style={{ padding: '20px 16px 16px' }}>
				<table className="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp food_list">
					<tbody>
						{listItem}
					</tbody>
				</table>


			</div>
			<div style={{ padding: '20px 16px 16px' }}>
				<p style={{ fontWeight: 'bold !important' }}>{`Date: ${this.props.order.date}`}</p>
				<p style={{ fontWeight: 'bold !important' }}>{`Time: ${5 + this.props.order.time * 2}:00 - ${7 + this.props.order.time * 2}:00`}</p>
				<p style={{ fontWeight: 'bold !important' }}>{`Table: ${this.props.table}`}</p>
				<p style={{ fontWeight: 'bold !important' }}>{`Number of People: ${this.props.order.number}`}</p>
				<p className="title">Total <span style={{ float: 'right' }}>{total}</span></p>

				<p className="title" style={{ marginTop: '15px' }}>Payment method<span style={{ float: 'right' }}>Cash</span></p>

			</div>





			<button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" data-upgraded=",MaterialButton,MaterialRipple" style={{
				marginTop: '15px', width: 'calc(100% - 0px)', background: '#2E4A9E !important'
			}} onClick={this.onClickConfirm.bind(this)} >
				PAYMENT
				< span className="mdl-button__ripple-container" > <span className="mdl-ripple is-animating" style={{ width: '159.541px', height: '159.541px', transform: 'translate(-50%, -50%) translate(24px, 30px)' }}></span>
				</span>
			</button>
		</div >
	}
}

export default connect()(Confirm)