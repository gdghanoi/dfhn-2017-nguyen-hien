import React from 'react'
import { connect } from 'react-redux'
import { Image, Row, Col } from 'react-bootstrap'
import firebase from 'firebase'

class Food extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			menu: null
		}
	}
	componentDidMount() {
		firebase.database().ref('/menu').once('value')
			.then(snap => snap.val())
			.then(data => {
				this.setState({ menu: data })
				this.menu = data
			})
	}
	onContinueClick(e) {
		this.props.setFood(this.state.menu)
	}
	render() {
		let listMenu1 = []
		let listMenu2 = []
		if (this.state.menu) {
			const menu1 = this.state.menu[1]
			const menu2 = this.state.menu[2]
			for (const [index, food] of menu1.entries()) {
				if (!food)
					continue
				const foodDiv = <Col xs={12} md={6} lg={3}>
					<Image src={food.imageUrl} responsive style={{ height: '200px', marginBottom: '20px' }} />
					<p style={{ marginTop: '15px' }}>{food.food_name}<select style={{ float: 'right' }}
						onChange={e => {
							const menu = this.state.menu
							menu[1][index].quantity = e.target.value
							this.setState({ menu })
						}}
						value={this.state.menu[1][index].quantity || 0}>
						<option value={0} >0</option>
						<option value={1}>1</option>
						<option value={2}>2</option>
						<option value={3}>3</option>
					</select></p>
					<p style={{ fontWeight: 'bold' }} >Price: {food.price}$</p>
				</Col>
				listMenu1.push(foodDiv)
			}
			for (const [index, food] of menu2.entries()) {
				if (!food)
					continue
				const foodDiv = <Col xs={12} md={6} lg={3}>
					<Image src={food.imageUrl} responsive style={{ height: '200px', marginBottom: '20px' }} />
					<p style={{ marginTop: '15px' }}>{food.food_name}<select style={{ float: 'right' }} onChange={e => {
						const menu = this.state.menu
						menu[2][index].quantity = e.target.value
						this.setState({ menu })
					}} value={this.state.menu[2][index].quantity || 0}>
						<option value={0}>0</option>
						<option value={1}>1</option>
						<option value={2}>2</option>
						<option value={3}>3</option>
					</select></p>
					<p style={{ fontWeight: 'bold' }} >Price: {food.price}$</p>
				</Col>
				listMenu2.push(foodDiv)
			}
		}
		return <div className="container" style={{ paddingTop: '20px' }}>
			<Row>
				<p style={{ fontSize: '30px', fontWeight: 'bold' }} >Foods</p>
				{listMenu1}
			</Row>
			<Row>
				<p style={{ fontSize: '30px', fontWeight: 'bold' }}>Drinks</p>
				{listMenu2}
			</Row>
			<button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
				onClick={this.onContinueClick.bind(this)}
				style={{ marginTop: '15px', width: 'calc(100% - 0px)', background: '#2E4A9E !important' }}>
				CONTINUE
			</button>
		</div >
	}
}

export default connect()(Food)