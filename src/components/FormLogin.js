import React from 'react'
import { setUser } from '../actions'
import { connect } from 'react-redux'
import { browserHistory, Redirect } from 'react-router'
import { auth } from '../firebase'
import '../social.css'

class FormLogin extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			email: '',
			password: ''
		}
	}
	handleEmail(e) {
		this.setState({ email: e.target.value })
	}
	handlePassword(e) {
		this.setState({ password: e.target.value })
	}
	onSubmit(e) {
		e.preventDefault()
		auth().signInWithEmailAndPassword(this.state.email, this.state.password)
			.then(user => {
				this.props.dispatch(setUser(user))
				window.location = '/'
			}).catch(() => {
				this.props.dispatch(setUser(null))
			})
	}
	componentWillMount() {
		if(localStorage.getItem('isLogin')==='true') {
			window.location = '/'
		}
	}

	render() {
		return <div className="animsition">
			<main className="mdl-layout__content login-bg">
				<div className="login mdl-shadow--2dp">
					<div className="account-info mdl-color--primary">

						<div className="account-navigation">

							<div className="clr"></div>

							<span>Log in</span>
						</div>
					</div>

					<div className="account-data" style={{ textAlign: 'center'}}>
						<form onSubmit={this.onSubmit.bind(this)}>
							<div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
								<input onChange={this.handleEmail.bind(this)} className="mdl-textfield__input" type="text" id="sample1" />
								<label className="mdl-textfield__label" for="sample1">Enter your email</label>
							</div>
							<div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
								<input onChange={this.handlePassword.bind(this)} className="mdl-textfield__input" type="password" id="sample2" />
								<label className="mdl-textfield__label" for="sample2">Password</label>
							</div>

							<button onClick={this.onSubmit.bind(this)} className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" data-upgraded=",MaterialButton,MaterialRipple" style={{ width: 'calc(100% - 50px)', margin: '0 25px', background: '#AD2B33' }}>
							Log in
								<span className="mdl-button__ripple-container"><span className="mdl-ripple is-animating" style={{ width: '159.541px', height: '159.541px', transform: 'translate(-50%, -50%) translate(24px, 30px)' }}></span>
								</span>
							</button>

							<div className="calendar-activity now" style={{ margin: '20px', float: 'none' }}>
								<div className="or_line" style={{ color: '#898278' }}>OR</div>
							</div>

							<button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" data-upgraded=",MaterialButton,MaterialRipple" style={{width: 'calc(100% - 50px)',margin: '0 25px', background: '#2E4A9E !important'}}>
							Sign up Facebook
								<span className="mdl-button__ripple-container"><span className="mdl-ripple is-animating" style={{ width: '159.541px', height: '159.541px', transform: 'translate(-50%, -50%) translate(24px, 30px)' }}></span>
								</span>
							</button>
						</form>

						<div className="weather-info">
							<p><a href="#" style={{ color: '#DAD6CB' }}>Sign up for Restaurant</a></p>
							<p><a href="#" style={{ color: '#DAD6CB' }}>Need help?</a></p>
						</div>


						<div className="clr"></div>
					</div>
				</div>
			</main>
		</div >
	}
}




export default connect()(FormLogin)