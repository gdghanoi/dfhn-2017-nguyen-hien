import React from 'react'
import { connect } from 'react-redux'
import firebase from 'firebase'

class History extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			orders: []
		}
	}
	componentDidMount() {
		if (localStorage.getItem('isLogin') === 'false') {
			window.location = '/login'
			return
		}
		firebase.database().ref(`/user/${localStorage.getItem('uid')}/orders`).once('value').then(snap => snap.val())
			.then(data => {
				this.setState({ orders: data })
			})

	}
	render() {
		const listOrder = []
		for (const orderKey of Object.keys(this.state.orders)) {
			const order = this.state.orders[orderKey]
			listOrder.push(<tr>
				<td class="mdl-data-table__cell--non-numeric" style={{ paddingLeft: '16px' }}>
					<div class="mdl-color-text--grey-600">
						<div class="card-author">
							<p>Ngày đặt bàn: <strong>{order['date']}</strong></p>
							<p>Tổng: <strong>{`${order['total']}$`}</strong></p>
							<p>Phương thức thanh toán: <strong>Cash</strong></p>
						</div>
					</div>
					<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" data-upgraded=",MaterialButton,MaterialRipple" style={{ marginTop: '15px;width: calc(100% - 0px)', background: '#2E4A9E !important', float: 'left' }}>
						Succeeded
						<span class="mdl-button__ripple-container"><span class="mdl-ripple is-animating" style={{ width: '159.541px', height: '159.541px', transform: 'translate(-50%, -50%) translate(24px, 30px)' }}></span>
						</span>
					</button>
				</td>
			</tr>)
		}
		return <div className="animsition">

			<div className="mdl-layout mdl-js-layout mdl-layout--overlay-drawer-button">
				<header className="mdl-layout__header mdl-layout__header--waterfall">
					<div className="mdl-layout__header-row">

						<div className="mdl-layout-spacer"></div>

						<button id="top-header-menu" className="mdl-button mdl-js-button mdl-button--icon">
							<i className="material-icons">Restaurant</i>
						</button>
					</div>
				</header>

				<ul className="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="top-header-menu">
					<a href="login.html" className="animsition-link"><li className="mdl-menu__item">Login</li></a>
					<a href="messages.html" className="animsition-link"><li className="mdl-menu__item mdl-badge" data-badge="4">Messages</li></a>
					<a href="player.html" className="animsition-link"><li className="mdl-menu__item">Player</li></a>
					<a href="calendar.html" className="animsition-link"><li className="mdl-menu__item">Calendar</li></a>
					<a href="settings.html" className="animsition-link"><li className="mdl-menu__item">Settings</li></a>
				</ul>

				<div className="mdl-layout__drawer">

					<div className="mdl-card mdl-shadow--2dp mdl-color--primary mdl-color-text--blue-grey-50 drawer-profile">
						<div className="mdl-card__title user">
							<img src="img/user.jpg" alt="" />
							<span className="user-mail">{this.props.email}</span>
						</div>
					</div>

					<nav className="mdl-navigation">
						<a className="mdl-navigation__link animsition-link"  href='/'><i className="material-icons">home</i><span>Order Now</span></a>
						<a className="mdl-navigation__link animsition-link" ><i className="material-icons">account_circle</i><span>Profile</span></a>
						<a className="mdl-navigation__link animsition-link" href='/history'><i className="material-icons">library_books</i><span>History</span></a>
						<div className="drawer-separator"></div>
						<a className="mdl-navigation__link mdl-collapse__button" href='/logout' ><i className="material-icons">apps</i>
							<span>Logout</span>
						</a>
					</nav>
				</div>


				<main className="mdl-layout__content">
					<div class="contact-about" style={{ padding: '20px 16px 16px' }}>
						<table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp food_list">
							<tbody>
								{listOrder}

							</tbody>
						</table>


					</div>
				</main>
			</div>
		</div >
	}
}

export default connect()(History)