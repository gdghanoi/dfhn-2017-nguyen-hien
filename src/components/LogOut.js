import React from 'react'
import { setUser } from '../actions'
import { connect } from 'react-redux'


class Logout extends React.Component {
	componentDidMount() {
		this.props.dispatch(setUser(null))
		window.location = '/login'
	}
	render() {
		return <div></div>
	}
}

export default connect()(Logout)