import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import { showFormLogin, setUser } from '../actions'
import { connect } from 'react-redux'


class Main extends React.Component {
	constructor(props) {
		super(props)
	}
	componentDidMount() {
		if (localStorage.getItem('isLogin') === 'true') {
			this.props.dispatch(setUser({ email: localStorage.getItem('email'), uid: localStorage.getItem('uid') }))
		}
	}
	render() {
		return <div>
			{this.props.children}
		</div>
	}
}

export default connect()(Main)
