import React from 'react'
import { connect } from 'react-redux'

// const startRecording = require('../../public/Record')

class Order extends React.Component {
	constructor(props) {
		super(props)
		const curtime = new Date()
		this.state = {
			number: 2,
			date: `${curtime.getFullYear()}-${curtime.getMonth() + 1}-${curtime.getDate()}`,
			time: 2
		}
	}
	handleDate(e) {
		this.setState({ date: e.target.value })
	}
	handleNumber(e) {
		this.setState({ number: parseInt(e.target.value) })
	}
	handleTime(e) {
		this.setState({ time: parseInt(e.target.value) })
	}
	onContinueClick(e) {
		this.props.setOrder(this.state)
	}
	render() {
		return <div className="mdl-grid">
			<div className="mdl-cell mdl-cell--12-col animsition">
				<main className="mdl-layout__content login-bg" style={{ background: 'transparent' }}>
					<p className="title title_order">People:</p>
					<div className="login mdl-shadow--2dp login_order">
						<div className="account-data" style={{ textAlign: 'center', padding: '0px 16px' }}>
							<form action="#" style={{ marginBottom: '0px' }}>
								<div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
									<select onChange={this.handleNumber.bind(this)} className="mdl-textfield__input" type="text" id="sample1" value={this.state.number}>
										<option value={1}>1 người</option>
										<option value={2}>2 người</option>
										<option value={3}>3 người</option>
										<option value={4}>4 người</option>
										<option value={5}>5 người</option>
										<option value={6}>6 người</option>
										<option value={7}>7 người</option>
										<option value={8}>8 người</option>
									</select>
								</div>
							</form>
						</div>
					</div>

					<p className="title title_order">Date:</p>
					<div className="login mdl-shadow--2dp login_order">
						<div className="account-data" style={{ textAlign: 'center', padding: '0px 16px' }}>
							<form action="#" style={{ marginBottom: '0px' }}>
								<div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
									<input className="mdl-textfield__input" type="date" onChange={this.handleDate.bind(this)} id="sample1" value={this.state.date} />
								</div>
							</form>
						</div>
					</div>

					<p className="title title_order">Time:</p>
					<div className="login mdl-shadow--2dp login_order">
						<div className="account-data" style={{ textAlign: 'center', padding: '0px 16px' }}>
							<form action="#" style={{ marginBottom: '0px' }}>
								<div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
									<select onChange={this.handleTime.bind(this)} id="select-time-search" className="mdl-textfield__input" value={this.state.time}>
										<option className="ng-binding ng-scope" value={1}>7:00 - 9:00</option>
										<option className="ng-binding ng-scope" value={2}>9:00 - 11:00</option>
										<option className="ng-binding ng-scope" value={3}>11:00 - 13:00</option>
										<option className="ng-binding ng-scope" value={4}>13:00 - 15:00</option>
										<option className="ng-binding ng-scope" value={5}>15:00 - 17:00</option>
										<option className="ng-binding ng-scope" value={6}>17:00 - 19:00</option>
										<option className="ng-binding ng-scope" value={7}>19:00 - 21:00</option>
										<option className="ng-binding ng-scope" value={8}>21:00 - 23:00</option>
									</select>

								</div>
							</form>
						</div>
					</div>

					<button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
						onClick={this.onContinueClick.bind(this)} style={{ marginTop: '15px', width: 'calc(100% - 0px)', background: '#2E4A9E !important' }}>
						CONTINUE
						<span className="mdl-button__ripple-container"><span className="mdl-ripple is-animating" style={{ width: '159.541px', height: '159.541px', transform: 'translate(-50%, -50%) translate(24px, 30px)' }}></span>
						</span>
					</button>

					<button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
						style={{ marginTop: '15px', width: 'calc(100% - 0px)', background: '#2E4A9E !important' }}>
						Order using your voice
						<span className="mdl-button__ripple-container"><span className="mdl-ripple is-animating" style={{ width: '159.541px', height: '159.541px', transform: 'translate(-50%, -50%) translate(24px, 30px)' }}></span>
						</span>
					</button>
				</main>
			</div>
		</div>
	}
}

export default connect()(Order)