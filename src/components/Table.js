import React from 'react'
import { connect } from 'react-redux'
import firebase from 'firebase'

class Table extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			table: null,
			tableId: null
		}
	}
	componentDidMount() {
		firebase.database().ref('/tables').once('value')
			.then(snap => snap.val())
			.then(table => {
				this.setState({ table })
			})
	}
	onClickTable(id) {
		this.setState({ tableId: id })
	}
	onClickContinue() {
		this.props.setTable(this.state.tableId)
	}
	render() {
		const listTable = []
		for (let i = 0; i < 9; i++) {
			let active = true
			if (this.state.table) {
				const orders = this.state.table[i + 1]['orders']
				if (orders) {
					for (const order of orders) {
						if (order['date'] == this.props.order.date && order['time_id'] == this.props.order.time) {
							active = false
							break
						}
					}
				}
			}
			let className = ''
			if (this.state.tableId == (i + 1))
				className = 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent selected'
			else if (active)
				className = 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent avaiable'
			else
				className = 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent inavaiable'
			const table = <div className="mdl-cell mdl-cell--4-col" key={i}>
				<div>
					<button className={className} onClick={!active ? null : () => {
						this.onClickTable(i + 1)
					}} style={{ height: '100%', width: '100%', fontSize: '25px' }}>
						{i + 1}
					</button>
				</div>
			</div>
			listTable.push(table)
		}
		return <div>
			<div className="mdl-grid gallery">
				<div className="mdl-cell mdl-cell--4-col">
					<div className="mdl-card__supporting-text mdl-color-text--grey-600" style={{ padding: '5px' }}>
						<div className="minilogo user2 inavaiable" style={{ marginRight: '10px' }}></div>
						<div className="card-author">
							<span><strong>Inavaiable</strong></span>
						</div>
					</div>
				</div>
				<div className="mdl-cell mdl-cell--4-col">
					<div className="mdl-card__supporting-text mdl-color-text--grey-600" style={{ padding: '5px' }}>
						<div className="minilogo user2 available" style={{ marginRight: '10px' }}></div>
						<div className="card-author">
							<span><strong>Avaiable</strong></span>
						</div>
					</div>
				</div>
				<div className="mdl-cell mdl-cell--4-col">
					<div className="mdl-card__supporting-text mdl-color-text--grey-600" style={{ padding: '5px' }}>
						<div className="minilogo user2 selected" style={{ marginRight: '10px' }}></div>
						<div className="card-author">
							<span><strong>Selected</strong></span>
						</div>
					</div>
				</div>
			</div>
			<p className="title title_order">List table:</p>
			<div className="mdl-grid gallery" style={{ marginTop: '20px' }}>
				{listTable}
			</div >


			<button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
				style={{ marginTop: '15px', width: 'calc(100% - 0px)', background: '#2E4A9E !important' }}
				onClick={this.onClickContinue.bind(this)}
			>
				CONTINUE
			</button>
		</div>
	}
}

export default connect()(Table)