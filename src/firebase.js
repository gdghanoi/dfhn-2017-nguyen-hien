import firebase from 'firebase'

const config = {
	apiKey: 'AIzaSyBOwa8RjZCPyBQ5gYnam2bmQ3GNyEmKnDM',
	authDomain: 'hackathon-37e00.firebaseapp.com',
	databaseURL: 'https://hackathon-37e00.firebaseio.com',
	projectId: 'hackathon-37e00',
	storageBucket: 'hackathon-37e00.appspot.com',
	messagingSenderId: '588002090063'
}
const init = () => firebase.initializeApp(config)

const auth = () => firebase.auth()
const database = () => firebase.database()

export default init
export { auth, database }