import { ActionTypes } from '../constants'

const setUser = (state = {}, action) => {
	switch (action.type) {
	case ActionTypes.SET_USER:
		if (action.user) {
			localStorage.setItem('isLogin', 'true')
			localStorage.setItem('email',action.user.email)
			localStorage.setItem('uid',action.user.uid)
			return {email:action.user.email, uid: action.user.uid}
		
		} 
		localStorage.setItem('isLogin', 'false')
		localStorage.setItem('email','')
		localStorage.setItem('uid','')
		return null
	default: return state
	}
}
const showFormLogin = (state = false, action) => {
	switch (action.type) {
	case ActionTypes.SHOW_FORM_LOGIN:
		return action.show
	default: return state
	}
}

export { setUser, showFormLogin }