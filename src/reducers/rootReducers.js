import { combineReducers } from 'redux'
import { setUser, showFormLogin } from './index'

const rootReducer = combineReducers({
	user: setUser,
	showFormLogin
})

export default rootReducer